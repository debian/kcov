#!/bin/sh

set -eu

SOURCE_DIR="$PWD"
TESTS_FOLDER="$(mktemp -d -t XXXXX_kcov-dep8)"
echo "Source in: $SOURCE_DIR"
echo "Test output in: $TESTS_FOLDER"

mkdir "$TESTS_FOLDER/tests-temp"

cd "$TESTS_FOLDER" && cmake $SOURCE_DIR/tests
cd "$TESTS_FOLDER" && make

cd $SOURCE_DIR && PYTHONPYCACHEPREFIX="$TESTS_FOLDER/py-cache" PYTHONPATH="$SOURCE_DIR/tests/tools" \
    python3 -m libkcov /usr/bin/kcov "$TESTS_FOLDER/tests-temp" "$TESTS_FOLDER/" "." --no-ptrace -v 2>/dev/stdout

rm -r $TESTS_FOLDER
