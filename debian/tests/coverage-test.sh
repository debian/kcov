#!/bin/sh

set -eu

COVERAGE_FOLDER="$(mktemp -d -t XXXXX_kcov-dep8-coverage)"
echo "Coverage output in: $COVERAGE_FOLDER"

/usr/bin/kcov --dump-summary --include-path=/usr/bin/pear "$COVERAGE_FOLDER" /usr/bin/pear list-channels > /dev/null
jq -r '.' "$COVERAGE_FOLDER/pear/coverage.json"

COVERED_LINES=$(jq -r '.covered_lines' "$COVERAGE_FOLDER/pear/coverage.json")
echo "Covered lines in pear: $COVERED_LINES"
if [ $COVERED_LINES -lt 8 ]; then
    echo "The covered lines in pear should be higher" > /dev/stderr
    exit 1
fi
